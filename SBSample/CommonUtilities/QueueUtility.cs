﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace CommonUtilities
{
    public static class QueueUtility
    {
        private static NamespaceManager nm = NamespaceManager.CreateFromConnectionString(CommonUtilities.ServiceBusSettings.ServiceBusConnectionString);
        //  private static QueueClient queueClient;
        private static void AddQueue(string QueueName)
        {
            if (!nm.QueueExists(QueueName))
            {
                nm.CreateQueue(QueueName);
            }
        }


        public static void RemoveQueue(string QueueName)
        {
            if (nm.QueueExists(QueueName))
            {
                nm.DeleteQueue(QueueName);
            }
        }
        public static void SendMessages(string QueueName, dynamic msg)
        {
            AddQueue(QueueName);
            var sendMessagingFactory = MessagingFactory.CreateFromConnectionString(ServiceBusSettings.ServiceBusConnectionString);
            var sender = sendMessagingFactory.CreateMessageSender(QueueName);
            sender.Send(CreateSampleMessage(msg));
        }

        private static BrokeredMessage CreateSampleMessage(dynamic messageBody)
        {

            var message = new BrokeredMessage(new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageBody))))
            {
                ContentType = "application/json",
                Label = "TravelBooking",
                TimeToLive = TimeSpan.FromMinutes(30),
                MessageId = Guid.NewGuid().ToString(),
            };
            return message;
        }

        private static object DeserializeTravelBooking(Stream body)
        {
            return JsonConvert.DeserializeObject(new StreamReader(body, true).ReadToEnd());
        }
        public static dynamic ReadMessageFromQueue()
        {
            var receiverMessagingFactory = MessagingFactory.CreateFromConnectionString(ServiceBusSettings.ServiceBusConnectionString);
            var receiver = receiverMessagingFactory.CreateMessageReceiver(ServiceBusSettings.FlightQueue);
            BrokeredMessage message = receiver.Receive();
            return DeserializeTravelBooking(message.GetBody<Stream>());
        }
    }
}
