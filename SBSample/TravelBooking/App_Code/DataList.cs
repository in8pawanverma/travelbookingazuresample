﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

/// <summary>
/// Summary description for DataList
/// </summary>
public static class DataList
{

    public static List<Car> GetCarList()
    {

        return new List<Car>()
        {

            new Car(){ CarID=1, CarName="SUV222" ,VendorID=1 },
            new Car(){ CarID=2, CarName="MARUTI122" ,VendorID=1},
            new Car(){ CarID=3, CarName="TATA243" ,VendorID=1},
            new Car(){ CarID=4, CarName="TATA56" ,VendorID=1},
            new Car(){ CarID=5, CarName="TSU3" ,VendorID=2},
            new Car(){ CarID=6, CarName="KDJF" ,VendorID=3},
            new Car(){ CarID=7, CarName="AK3" ,VendorID=2},
            new Car(){ CarID=8, CarName="3ASD23" ,VendorID=3},
            new Car(){ CarID=9, CarName="AS23" ,VendorID=4},
            new Car(){ CarID=10, CarName="AD33",VendorID=4 },
             new Car(){ CarID=11, CarName="VEEE",VendorID=5 },
              new Car(){ CarID=12, CarName="EFFD",VendorID=6 },
               new Car(){ CarID=13, CarName="232",VendorID=7 },
                new Car(){ CarID=14, CarName="ASDF2",VendorID=8 },
                 new Car(){ CarID=15, CarName="JFGF",VendorID=8 },
                  new Car(){ CarID=16, CarName="3234",VendorID=9 },
                   new Car(){ CarID=17, CarName="AFASDF",VendorID=10 }
        };
    }


    public static List<Vendor> GetVendorList()
    {
        return new List<Vendor>()
        {
            new Vendor(){ VendorID=1,  VendorName="Vendor1" },
            new Vendor(){ VendorID=2,  VendorName="Vendor2" },
            new Vendor(){ VendorID=3,  VendorName="Vendor3" },
            new Vendor(){ VendorID=4,  VendorName="Vendor4" },
            new Vendor(){ VendorID=5,  VendorName="Vendor5" },
            new Vendor(){ VendorID=6,  VendorName="Vendor6" },
            new Vendor(){ VendorID=7,  VendorName="Vendor7" },
            new Vendor(){ VendorID=8,  VendorName="Vendor8" },
            new Vendor(){ VendorID=9,  VendorName="Vendor9" },
            new Vendor(){ VendorID=10, VendorName="Vendor10" }
        };
    }

    public static List<AirPort> GetAirPortList()
    {
        return new List<AirPort>()
        {
            new AirPort(){ AirPortID=1,  AirPortName="BLR" },
            new AirPort(){ AirPortID=2, AirPortName="MAA" },
            new AirPort(){ AirPortID=3, AirPortName="DLH" },
            new AirPort(){ AirPortID=4, AirPortName="KLK" },
            new AirPort(){ AirPortID=5, AirPortName="LKN" },
            new AirPort(){ AirPortID=6, AirPortName="CHD" },
            new AirPort(){ AirPortID=7, AirPortName="AMR" },
            new AirPort(){ AirPortID=8, AirPortName="ODD" },
            new AirPort(){ AirPortID=9, AirPortName="MUM" },
            new AirPort(){ AirPortID=10,AirPortName="CHN" }
        };
    }




    public class Car
    {
        public Int32 CarID { get; set; }
        public String CarName { get; set; }

        public Int32 VendorID { get; set; }
    }

    public class Vendor
    {
        public Int32 VendorID { get; set; }
        public String VendorName { get; set; }
    }

    public class AirPort
    {
        public Int32 AirPortID { get; set; }
        public String AirPortName { get; set; }
    }

    #region Hotel Block
   
    public static List<HotelName> GetHotelList()
    {
        return new List<HotelName>()
        {
             new HotelName(){name = "Hopeman", city = "Kirkland", state = "Hyderabad", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2)},
            new HotelName(){ name = "Hopeman", city = "Kirkland", state = "KArnatka", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2)},
            new HotelName(){name = "Hopeman", city = "Kirkland", state = "odisha", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2) },
            new HotelName(){ name = "sdsd", city = "Kirkland", state = "Punjab", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2) },
            new HotelName(){ name = "sdee", city = "Kirkland", state = "bihar", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2)},
               new HotelName(){ name = "dsd", city = "Kirkland", state = "KArnatka", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2)},
            new HotelName(){name = "Hopesdsman", city = "Kirkland", state = "odisha", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2) },
            new HotelName(){ name = "dddd", city = "Kirkland", state = "odisha", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2) },
               new HotelName(){ name = "Hopeman", city = "Kirkland", state = "KArnatka", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2)},
            new HotelName(){name = "sds", city = "Kirkland", state = "KArnatka", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2) },
            new HotelName(){ name = "sd", city = "Kirkland", state = "KArnatka", checkin = DateTime.Now, checkout = DateTime.Now.AddDays(2) },


        };
    }


    public class HotelName
    {
        public string name { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public DateTime checkin { get; set; }
        public DateTime checkout { get; set; }

    }

    #endregion
}