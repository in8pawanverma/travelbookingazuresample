﻿using System;
using System.Linq;
using CommonUtilities;
public partial class _Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindVendorList();
            ddlCar.Enabled = false;
            bindAirPortList();
            bindHotelList();
            bindHotelState();
        }
    }


    public void bindCarList(Int32 id)
    {
        ddlCar.Enabled = true;
        ddlCar.DataSource = DataList.GetCarList().Where(R => R.VendorID == id);
        ddlCar.DataTextField = "CarName";
        ddlCar.DataValueField = "CarID";
        ddlCar.DataBind();
    }
    public void bindAirPortList()
    {
        ddlAirPort.Enabled = true;
        ddlAirPort.DataSource = DataList.GetAirPortList();
        ddlAirPort.DataTextField = "AirPortName";
        ddlAirPort.DataValueField = "AirPortID";
        ddlAirPort.DataBind();
    }

    public void bindVendorList()
    {
        ddlVendor.DataSource = DataList.GetVendorList();
        ddlVendor.DataTextField = "VendorName";
        ddlVendor.DataValueField = "VendorID";
        ddlVendor.DataBind();
    }

    protected void btnBookCar_Click(object sender, EventArgs e)
    {
        if (ddlCar.SelectedValue != null
              && ddlVendor.SelectedValue != null
              && ddlAirPort.SelectedValue != null
              && !string.IsNullOrEmpty(dtFrom.Text)
              && !string.IsNullOrEmpty(dtUntil.Text))
        {
            dynamic bookingRequests = new dynamic[]
               {
              new
                {
                    car = new {
                        car= ddlCar.SelectedItem.Value.ToUpper(),
                        vendor = ddlVendor.SelectedItem.Text.ToUpper(),
                        airport = ddlAirPort.SelectedItem.Text.ToUpper(),
                        from = dtFrom.Text,
                        until = dtUntil.Text
                    }
                }
               };
            QueueUtility.SendMessages(CommonUtilities.ServiceBusSettings.CarQueue, bookingRequests);

        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "PopupScript", "alert('Please fill all fields')", true);
        }

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindCarList(Convert.ToInt32(ddlVendor.SelectedValue));
    }
    protected void btnCancelCar_Click(object sender, EventArgs e)
    {

    }
    protected void btnBookFlight_Click(object sender, EventArgs e)
    {
        var Ffrom = txtFromPlace.Text;
        var Fto = txtToPlace.Text;
        var Fcheckin = txtdatestart.Text;
        var Fcheckout = txtdateend.Text;
        var BookingClass = DropDownList1.Text;


        if (ddlCar.SelectedValue != null
          && ddlVendor.SelectedValue != null
          && ddlAirPort.SelectedValue != null
          && !string.IsNullOrEmpty(dtFrom.Text)
          && !string.IsNullOrEmpty(dtUntil.Text))
        {

            dynamic bookingRequests = new dynamic[]
             {
              new
                {
                    car = new {
                        FromLocation=  txtFromPlace.Text,
                        ToLocation = txtToPlace.Text,
                        CheckInDate = txtdatestart.Text,
                        CheckOutDate = txtdateend.Text,
                        Class = DropDownList1.Text
                    }
                }
             };



            QueueUtility.SendMessages(ServiceBusSettings.FlightQueue, bookingRequests);

        }
        else
        {
            ClientScript.RegisterStartupScript(GetType(), "PopupScript", "alert('Please fill all fields for flight booking')", true);
        }

    }
    protected void btnBookTrip_Click(object sender, EventArgs e)
    {

       
    }

    #region Hotel Block
    public void bindHotelList()
    {
        HotelName.DataSource = DataList.GetHotelList();
        HotelName.DataTextField = "name";
        HotelName.DataValueField = "name";
        HotelName.DataBind();
    }




    public void bindHotelName()
    {
        HotelCity.DataSource = DataList.GetHotelList();
        HotelCity.DataTextField = "name";
        HotelCity.DataValueField = "name";
        HotelCity.DataBind();
    }

    public void bindHotelCity(String state)
    {
        HotelCity.DataSource = DataList.GetHotelList().Where(R => R.state == state);
        HotelCity.DataTextField = "city";
        HotelCity.DataValueField = "city";
        HotelCity.DataBind();
    }

    public void bindHotelState()
    {
        HotelState.DataSource = DataList.GetHotelList();
        HotelState.DataTextField = "state";
        HotelState.DataValueField = "state";
        HotelState.DataBind();
    }

    protected void ddlHotelState_SelectedIndexChanged(object sender, EventArgs e)
    {
        //bindHotelState();
        bindHotelCity(Convert.ToString(HotelState.SelectedValue));

    }

    protected void ddlHotelCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        //  bindHotelCity(Convert.ToString(HotelState.SelectedValue));
    }
    protected void btnCancelhotel_Click(object sender, EventArgs e)
    {

    }
    protected void btnBookhotel_Click(object sender, EventArgs e)
    {

    }
    #endregion

}