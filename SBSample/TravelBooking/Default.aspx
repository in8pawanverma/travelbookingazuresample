﻿<%@ Page Language="C#" CodeFile="~/Default.aspx.cs"
    Inherits="_Default" AutoEventWireup="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Travel Booking</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

</head>
<body>
    <form id="form1" runat="server">

        <div class="container-fluid">
            <h2>Travel Booking</h2>

            <div class="row">
                <ul class="nav nav-tabs nav-justified " role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab"><i class="fa fa-user"></i>Car Booking</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel2" role="tab"><i class="fa fa-heart"></i>Hotel Booking</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel3" role="tab"><i class="fa fa-envelope"></i>Flight Booking</a>
                    </li>
                </ul>
                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 1-->
                    <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                        <div class="container">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Choose Vendor</label>
                                    <asp:DropDownList runat="server" CssClass="form-control" ClientIDMode="Static" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Choose Car</label>
                                    <asp:DropDownList runat="server" CssClass="form-control" ClientIDMode="Static" ID="ddlCar">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Destination</label>
                                    <asp:DropDownList runat="server" CssClass="form-control" ClientIDMode="Static" ID="ddlAirPort">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From</label>
                                    <asp:TextBox runat="server" ID="dtFrom" CssClass="form-control" ClientIDMode="Static" />

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">End</label>
                                    <asp:TextBox runat="server" ID="dtUntil" CssClass="form-control" ClientIDMode="Static" />
                                </div>
                                <asp:Button Text="BookCar" ClientIDMode="Static" ID="btnBookCar" CssClass="btn btn-success" OnClick="btnBookCar_Click" runat="server" />
                                <asp:Button Text="Cancel" ClientIDMode="Static" ID="btnCancelCar" CssClass="btn btn-default" OnClick="btnCancelCar_Click" runat="server" />
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>
                    <!--/.Panel 1-->

                    <!--Panel 2-->
                    <div class="tab-pane fade" id="panel2" role="tabpanel">
                        <div class="container">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hotel Name</label>
                                <asp:DropDownList runat="server" Name="HotelName" CssClass="form-control" ClientIDMode="Static" ID="HotelName">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">State</label>
                                <asp:DropDownList runat="server" name="HotelState" CssClass="form-control" ClientIDMode="Static" ID="HotelState" OnSelectedIndexChanged="ddlHotelState_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">City</label>
                                <asp:DropDownList runat="server" CssClass="form-control" name="HotelCity" ClientIDMode="Static" ID="HotelCity" OnSelectedIndexChanged="ddlHotelCity_SelectedIndexChanged" AutoPostBack="false">
                                </asp:DropDownList>
                            </div>
                            <%--<div class="form-group">
                                    <label for="exampleInputEmail1">CheckinDate</label>
                                    <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
                                </div>--%>
                            <div class="form-group">
                                <label for="hotelFromDate">Check In</label>
                                <asp:TextBox runat="server" ID="Calendar1" CssClass="form-control" ClientIDMode="Static" />
                            </div>
                            <%--<div class="form-group">
                                    <label for="exampleInputEmail1">CheckOutDate</label>
                                    <asp:Calendar ID="Calendar2" runat="server"></asp:Calendar>
                                </div>--%>
                            <div class="form-group">
                                <label for="hotelTODate">Check Out</label>
                                <asp:TextBox runat="server" ID="Calendar2" CssClass="form-control" ClientIDMode="Static" />
                            </div>

                            <asp:Button Text="BookHotel" ClientIDMode="Static" ID="Button1" CssClass="btn btn-success" OnClick="btnBookhotel_Click" runat="server" />

                            <asp:Button Text="Cancel" ClientIDMode="Static" ID="Button2" CssClass="btn btn-default" OnClick="btnCancelhotel_Click" runat="server" />

                        </div>

                    </div>
                    <!--/.Panel 2-->

                    <!--Panel 3-->
                    <div class="tab-pane fade" id="panel3" role="tabpanel">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-field">
                                    <label for="from">From:</label>
                                    <asp:TextBox type="text" runat="server" class="form-control" ID="txtFromPlace" placeholder="Los Angeles, USA" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-field">
                                    <label for="from">To:</label>
                                    <asp:TextBox runat="server" type="text" class="form-control" ID="txtToPlace" placeholder="Tokyo, Japan" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-field">
                                    <label for="date-start">Check In:</label>
                                    <asp:TextBox type="text" runat="server" class="form-control" ID="txtdatestart" ClientIDMode="Static" placeholder="mm/dd/yyyy" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-field">
                                    <label for="date-end">Check Out:</label>
                                    <asp:TextBox runat="server" type="text" class="form-control" ID="txtdateend" ClientIDMode="Static" placeholder="mm/dd/yyyy" />
                                </div>
                            </div>
                            <div class="form-group">
                                <section>
                                    <label for="class">Class:</label>

                                </section>
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="economy" Value="red"></asp:ListItem>
                                    <asp:ListItem Text="first" Value="blue"></asp:ListItem>
                                    <asp:ListItem Text="business" Value="green"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <%--<inkput type="submit" class="btn btn-primary btn-block" value="Booking Flight" />--%>
                                <asp:Button ID="btnBookFlight" runat="server" class="btn btn-primary btn-block" Text="Booking Flight" OnClick="btnBookFlight_Click" />
                            </div>

                        </div>
                    <div class="col-md-2">
                    </div>
                        </div>
                </div>
                <!--/.Panel 3-->
            </div>
        </div>
       
    </form>
    <script type="text/javascript">
        $(function () {
            $("#dtFrom").datepicker();
            $("#dtUntil").datepicker();
            $("#txtdatestart").datepicker();
            $("#txtdateend").datepicker();
            $("#Calendar1").datepicker();
            $("#Calendar2").datepicker();
        });
        $('.nav-link').on('click', function () {
            $('.nav-item').removeClass('active');
            $('.tab-pane').removeClass('active');
            $('.tab-pane').removeClass('show');
        });
    </script>
</body>
</html>
