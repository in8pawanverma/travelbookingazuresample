﻿using Microsoft.ServiceBus.Messaging;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using CommonUtilities;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Text;
using System.Transactions;
using Microsoft.ServiceBus;
using System.Linq;
namespace TravelTransactionHandler
{
    static class TravelBooking
    {
        public static async Task BookCar(BrokeredMessage message, MessageSender nextStepQueue, MessageSender compensatorQueue)
        {
            try
            {
                var via = (message.Properties.ContainsKey("Via") ? ((string)message.Properties["Via"] + ",") : string.Empty) + "bookcar";
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (message.Label != null &&
                        message.ContentType != null &&
                        message.Label.Equals(ServiceBusSettings.TravelBookingLabel, StringComparison.InvariantCultureIgnoreCase) &&
                        message.ContentType.Equals(ServiceBusSettings.ContentTypeApplicationJson, StringComparison.InvariantCultureIgnoreCase))
                    {
                        var body = message.GetBody<Stream>();
                        dynamic travelBooking = DeserializeTravelBooking(body);

                        // do we want to book a flight? No? Let's just forward the message to
                        // the next destination via transfer queue
                        if (travelBooking.car == null)
                        {
                            await nextStepQueue.SendAsync(CreateForwardMessage(message, travelBooking, via));
                            // done with this job
                            await message.CompleteAsync();
                        }
                        else
                        {
                            lock (Console.Out)
                            {
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("Booking Rental Car");
                                Console.ResetColor();
                            }

                            // now we're going to simulate the work of booking a car,
                            // which usually involves a call to a third party

                            // every 13th car booking sadly goes wrong
                            if (message.SequenceNumber % 13 == 0)
                            {
                                await message.DeadLetterAsync(
                                    new Dictionary<string, object>
                                    {
                                        {"DeadLetterReason", "TransactionError"},
                                        {"DeadLetterErrorDescription", "Failed to perform rental car reservation"},
                                        {"Via", via}
                                    });
                            }
                            else
                            {
                                // every operation executed in the first 3 secs of any minute 
                                // tanks completely (simulates some local or external unexpected issue) 
                                if (DateTime.UtcNow.Second <= 3)
                                {
                                    throw new Exception("O_o");
                                }

                                // let's pretend we booked something
                                travelBooking.car.reservationId = "QP271713299R";

                                await nextStepQueue.SendAsync(CreateForwardMessage(message, travelBooking, via));

                                // done with this job
                                await message.CompleteAsync();
                            }
                        }
                    }
                    else
                    {
                        await message.DeadLetterAsync(
                            new Dictionary<string, object>
                                    {
                                        {"DeadLetterReason", "BadMessage"},
                                        {"DeadLetterErrorDescription", "Unrecognized input message"},
                                        {"Via", via}
                                    });
                    }
                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                await message.AbandonAsync();
            }
        }
        public static async Task CancelRentalCar(BrokeredMessage message, MessageSender nextStepQueue, MessageSender compensatorQueue)
        {
            try
            {
                var via = (message.Properties.ContainsKey("Via")
                    ? ((string)message.Properties["Via"] + ",")
                    : string.Empty) +
                          "cancelcar";

                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (message.Label != null &&
                        message.ContentType != null &&
                        message.Label.Equals(ServiceBusSettings.TravelBookingLabel, StringComparison.InvariantCultureIgnoreCase) &&
                        message.ContentType.Equals(ServiceBusSettings.ContentTypeApplicationJson, StringComparison.InvariantCultureIgnoreCase))
                    {
                        var body = message.GetBody<Stream>();
                        dynamic travelBooking = DeserializeTravelBooking(body);

                        // do we want to book a flight? No? Let's just forward the message to
                        // the next destination via transfer queue
                        if (travelBooking.car != null &&
                            travelBooking.car.reservationId != null)
                        {
                            lock (Console.Out)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Cancelling Rental Car");
                                Console.ResetColor();
                            }

                            // undo the reservation (or pretend to fail)
                            if (DateTime.UtcNow.Second <= 3)
                            {
                                throw new Exception("O_o");
                            }

                            // reset the id
                            travelBooking.car.reservationId = null;

                            // forward
                            await nextStepQueue.SendAsync(CreateForwardMessage(message, travelBooking, via));
                        }
                        else
                        {
                            await nextStepQueue.SendAsync(CreateForwardMessage(message, travelBooking, via));
                        }
                        // done with this job
                        await message.CompleteAsync();
                    }
                    else
                    {
                        await message.DeadLetterAsync(
                            new Dictionary<string, object>
                                    {
                                        {"DeadLetterReason", "BadMessage"},
                                        {"DeadLetterErrorDescription", "Unrecognized input message"},
                                        {"Via", via}
                                    });
                    }
                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                await message.AbandonAsync();
            }
        }

        static BrokeredMessage CreateForwardMessage(BrokeredMessage message, dynamic travelBooking, string via)
        {
            var brokeredMessage = new BrokeredMessage(SerializeTravelBooking(travelBooking))
            {
                ContentType = ServiceBusSettings.ContentTypeApplicationJson,
                Label = message.Label,
                TimeToLive = message.ExpiresAtUtc - DateTime.UtcNow
            };
            foreach (var prop in message.Properties)
            {
                brokeredMessage.Properties[prop.Key] = prop.Value;
            }
            brokeredMessage.Properties["Via"] = via;
            return brokeredMessage;
        }

        static MemoryStream SerializeTravelBooking(dynamic travelBooking)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(travelBooking)));
        }
        private static dynamic DeserializeTravelBooking(Stream body)
        {
            return JsonConvert.DeserializeObject(new StreamReader(body, true).ReadToEnd());
        }

        

       
    }
}
