﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace TravelTransactionHandler
{
    public static class CreateQueue
    {
        private static NamespaceManager nm = NamespaceManager.CreateFromConnectionString(CommonUtilities.ServiceBusSettings.ServiceBusConnectionString);
        private static QueueClient queueClient;
        public static void AddQueue(string QueueName)
        {
            Console.WriteLine("\nCreating Queue '{0}'...", QueueName);
            if (nm.QueueExists(QueueName))
            {
                Console.WriteLine("\nDeleted Queue '{0}'...", QueueName);
                nm.DeleteQueue(QueueName);
            }
            Console.WriteLine("\nCreated Queue '{0}'...", QueueName);
            nm.CreateQueue(QueueName);
        }
        private static void SendMessages(string QueueName)
        {
            queueClient = QueueClient.Create(QueueName);
            List<BrokeredMessage> messageList = new List<BrokeredMessage>
            {
                CreateSampleMessage("1", "First message information"),
                CreateSampleMessage("2", "Second message information"),
                CreateSampleMessage("3", "Third message information")
            };

            Console.WriteLine("\nSending messages to Queue...");

            foreach (BrokeredMessage message in messageList)
            {
                while (true)
                {
                    try
                    {
                        queueClient.Send(message);
                    }
                    catch (MessagingException e)
                    {
                        if (!e.IsTransient)
                        {
                            Console.WriteLine(e.Message);
                            throw;
                        }
                        else
                        {
                            HandleTransientErrors(e);
                        }
                    }
                    Console.WriteLine(string.Format("Message sent: Id = {0}, Body = {1}", message.MessageId, message.GetBody<string>()));
                    break;
                }
            }

        }

        private static BrokeredMessage CreateSampleMessage(string messageId, string messageBody)
        {

            var message = new BrokeredMessage(new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject())))
            {
                ContentType = "application/json",
                Label = "TravelBooking",
                TimeToLive = TimeSpan.FromMinutes(15),
                MessageId = messageId
            };
            return message;
        }

        private static void HandleTransientErrors(MessagingException e)
        {
            //If transient error/exception, let's back-off for 2 seconds and retry 
            Console.WriteLine(e.Message);
            Console.WriteLine("Will retry sending the message in 2 seconds");
            Thread.Sleep(2000);
        }
    }
}
